package com.folcademy.bancocarubin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoCarubinApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoCarubinApplication.class, args);
	}

}
